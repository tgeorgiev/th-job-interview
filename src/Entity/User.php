<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $courseViewedAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $course_views;

    public function getCourseViewedAt(): ?\DateTimeInterface
    {
        return $this->courseViewedAt;
    }

    public function setCourseViewedAt(?\DateTimeInterface $courseViewedAt): self
    {
        $this->courseViewedAt = $courseViewedAt;

        return $this;
    }

    public function getCourseViews(): ?int
    {
        return $this->course_views;
    }

    public function setCourseViews(?int $course_views): self
    {
        $this->course_views = $course_views;

        return $this;
    }
}