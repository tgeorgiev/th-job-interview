<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\User;
use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to view course contents in the public part of the site.
 *
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/", defaults={"page": "1", "_format"="html"}, methods={"GET"}, name="course_index")
     * @Route("/page/{page<[1-9]\d*>}", defaults={"_format"="html"}, methods={"GET"}, name="course_index_paginated")
     */
    public function index(Request $request, int $page, string $_format, CourseRepository $courses): Response
    {
        $latestCourses = $courses->findLatest($page);
        return $this->render('course/index.'.$_format.'.twig', ['courses' => $latestCourses]);
    }

    /**
     * @Route("/course/{id}", methods={"GET"}, name="course_view")
     */
    public function courseView(Course $course): Response
    {
        if($this->isGranted('view', $course)) {
            $viewAllowed = true;
            $this->getDoctrine()
                ->getRepository(User::class)
                ->updateCourseViewCounters($this->getUser());
        } else {
            $viewAllowed = false;
        }

        return $this->render('course/view.html.twig', ['course' => $course, 'viewAllowed' => $viewAllowed]);
    }

}