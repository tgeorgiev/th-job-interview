<?php

namespace App\Security;

use App\Entity\Course;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use DateTime;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CourseVoter extends Voter
{
    // Type of permissions
    const VIEW = 'view';
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager, ParameterBagInterface $params)
    {
        $this->decisionManager = $decisionManager;
        $this->params = $params;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW))) {
            return false;
        }

        // only vote on Course objects inside this voter
        if (!$subject instanceof Course) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // ROLE_SUPER_ADMIN and ROLE_ADMIN can view the courses
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'))) {
            return true;
        }

        // you know $subject is a Course object, thanks to supports
        /** @var Course $course */
        $course = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($course, $user);
            default:
                throw new \LogicException('This code should not be reached!');
        }
    }

    private function canView(User $user)
    {
        $courseViews = $user->getCourseViews();
        $courseViewedAt = $user->getCourseViewedAt();
        $now = new DateTime("now");
        $gracefulPeriod = $this->params->get('gracefulPeriod');
        $courseViewLimits = $this->params->get('courseViewLimits');

        // If more than one day has passed since the last viewed course
        if($courseViewedAt) {
            $diffInSeconds = $now->getTimestamp() - $courseViewedAt->getTimestamp();
            if($diffInSeconds >= $gracefulPeriod) {
                return true;
            }
        }

        // If the user has watched less than 10 courses
        if($courseViews < $courseViewLimits) {
            return true;
        }
    }
}