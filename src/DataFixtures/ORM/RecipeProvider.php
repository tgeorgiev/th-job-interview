<?php

namespace App\DataFixtures\ORM;

use Faker\Provider\Base as BaseProvider;
use Faker\Generator;

class RecipeProvider extends BaseProvider
{
    public function recipeName()
    {
        $key = array_rand($this->recipeYoutubeNames);
        return $this->recipeYoutubeNames[$key];
    }

    public function recipeVideo()
    {
        $key = array_rand($this->recipeYoutubeIDs);
        return $this->recipeYoutubeIDs[$key];
    }

    private $recipeYoutubeNames = [
        "Slow Cooker Pineapple Baby Back Ribs",
        "Crispy Cheesy Hash Brown Egg Bake",
        "Easy And Refreshing Dinners",
        "13 Easy Microwave Cake Recipes",
        "Giant Chocolate Souffle: Behind Tasty",
        "Monkey Bread Brie Fondue",
        "Boozy Rainbow Sherbet Punch",
        "Sheet-Pan Crispy Chicken Strips and Veggies",
        "Sheet Pan Stuffed Pastry Pockets",
        "Slow Cooker Turkey Chili with Cornbread Dumplings",
    ];

    private $recipeYoutubeIDs = [
        "v-p33eIVCa8",
        "47wqjt6wmkE",
        "3vMWMq6wVTs",
        "rMjbRnBl8rw",
        "PkHotdob_08",
        "qbOYUaxvKGA",
        "c-YF2tmUgKA",
        "giTfjD93Ym0",
        "7PkssJIBZaE",
        "zLRW9lmFG2Y",
    ];
}