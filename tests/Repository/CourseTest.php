<?php

namespace App\Tests\Repository;

use App\Entity\Course;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CourseTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindLatest()
    {
        $courses = $this->entityManager
            ->getRepository(Course::class)
            ->findLatest()
        ;

        $this->assertCount(30, $courses);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}