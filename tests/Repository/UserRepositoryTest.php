<?php

namespace App\Tests\Repository;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class UserRepositoryTest extends WebTestCase
{
    public function testUserCreated()
    {
        self::bootKernel();

        // returns the real and unchanged service container
        $container = self::$kernel->getContainer();

        // gets the special container that allows fetching private services
        $container = self::$container;

        // First user should has a password 12345 to pass this test
        //$user = self::$container->get('doctrine')->getRepository(User::class)->findOneByEmail('test@test.com');
        $user = self::$container->get('doctrine')->getRepository(User::class)->find(1);
        $this->assertTrue(self::$container->get('security.password_encoder')->isPasswordValid($user, '12345'));

    }
}