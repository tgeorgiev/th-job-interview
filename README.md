## Intro
![project screenshot][screenshot]

**This project is using Symfony v4 Standard Edition. It is just a working version without any styles.**

![project screenshot][sonar_screenshot]

![project screenshot][sonar_screenshot2]
 After Unit tests the coverage is 47.4% only on the important code.
 Overall it was needed 311 lines of code for the task

## Installation
1. Clone the project from the repo
    ```
    git clone https://tgeorgiev@bitbucket.org/tgeorgiev/th-job-interview.git
    ```
2. enter in the project `cd th-job-interview`
3. install everything via composer `composer install`
4. update `.env` file with DATABASE_URL like
    ```
    DATABASE_URL=mysql://root:@127.0.0.1:3306/th_interview
    ```
4. create the DB `th_interview` with `php bin/console doctrine:database:create`
5. migrate the DB `php bin/console doctrine:migrations:migrate`
6. load the courses with fixtures `php bin/console doctrine:fixtures:load`
7. start a server `php bin/console server:run`

## Useful Commands
* Create a test user via the console
    ```
    php bin/console fos:user:create testuser
    ```
* promote a user with admin role
    ```
    php bin/console fos:user:promote testuser ROLE_ADMIN
    ```
* run unit tests. You should have created one user in order to pass all tests. You should also setup `DATABASE_URL` in `phpunit.xml.dist`
    ```
    ./bin/phpunit --log-junit 'reports/unitreport.xml' --coverage-clover 'reports/clover.xml' --coverage-html 'reports/clover_html' tests/
    ```

## Useful Links
* home page [http://127.0.0.1:8000](http://127.0.0.1:8000)
* user login [http://127.0.0.1:8000/login](http://127.0.0.1:8000/login) *the user can login with username or email*
* user register [http://127.0.0.1:8000/register](http://127.0.0.1:8000/register)
* user logout [http://127.0.0.1:8000/logout](http://127.0.0.1:8000/logout)

## Comments
1. The `courseViews` and `courseViewedAt` counters are in the DB and tracked per user
2. Parameters of the app (`gracefulPeriod` and `courseViewLimits`) can be configured at `.\config\services.yaml`

[screenshot]: https://bitbucket.org/tgeorgiev/th-job-interview/raw/master/public/img/screenshot.png "Screenshot"
[sonar_screenshot]: https://bitbucket.org/tgeorgiev/th-job-interview/raw/master/public/img/sonar_screenshot.png "Screenshot"
[sonar_screenshot2]: https://bitbucket.org/tgeorgiev/th-job-interview/raw/master/public/img/sonar_screenshot2.png "Screenshot"